import { useState } from "react";
import {
  createTheme,
  PaletteMode,
  Box,
  Container,
  Stack,
  ThemeProvider,
} from "@mui/material";
import Rightbar from "./components/Rightbar";
import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import Feed from "./components/Feed";

function App() {
  const [mode, setMode] = useState<PaletteMode>("light");

  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });

  return (
    <ThemeProvider theme={darkTheme}>
      <Box bgcolor={"background.default"} color={"text.primary"}>
        <Navbar />
        <Stack
          direction={"row"}
          spacing={{ xs: 0, sm: 2, md: 2, lg: 2, xl: 2 }}
          display={"flex"}
          justifyContent={"space-between"}
        >
          <Sidebar setMode={setMode} />
          <Feed />
          <Rightbar />
        </Stack>
      </Box>
    </ThemeProvider>
  );
}

export default App;
