import {
  AccountBox,
  Article,
  ChangeHistory,
  Group,
  Inbox,
  ModeNight,
  Person,
  Settings,
  Storefront,
} from "@mui/icons-material";
import {
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Stack,
  Switch,
  PaletteMode,
} from "@mui/material";
import React, { ElementType } from "react";
import Home from "@mui/icons-material/Home";
import SidebarOption from "./SidebarOption";
import Add from "./Add";

type SidebarProps = {
  setMode: React.Dispatch<React.SetStateAction<PaletteMode>>;
};

const Sidebar = ({ setMode }: SidebarProps) => {
  return (
    <Box flex={1} p={2} sx={{ display: { xs: "none", sm: "block" } }}>
      <Box position={"fixed"}>
        <List>
          <SidebarOption
            name={"Homepage"}
            href={"#home"}
            icon={
              <div>
                <Home />
              </div>
            }
          />
          <SidebarOption
            name={"Pages"}
            href={"#pages"}
            icon={
              <div>
                <Article />
              </div>
            }
          />
          <SidebarOption
            name={"Groups"}
            href={"#groups"}
            icon={
              <div>
                <Group />
              </div>
            }
          />
          <SidebarOption
            name={"Marketplace"}
            href={"#marketplace"}
            icon={
              <div>
                <Storefront />
              </div>
            }
          />
          <SidebarOption
            name={"Friends"}
            href={"#friends"}
            icon={
              <div>
                <Person />
              </div>
            }
          />
          <SidebarOption
            name={"Settings"}
            href={"#settings"}
            icon={
              <div>
                <Settings />
              </div>
            }
          />
          <SidebarOption
            name={"Profile"}
            href={"#profile"}
            icon={
              <div>
                <AccountBox />
              </div>
            }
          />

          <ListItem disablePadding>
            <ListItemButton component="a">
              <ListItemIcon>
                <ModeNight />
              </ListItemIcon>
              <Switch
                onClick={() =>
                  setMode((prevMode: PaletteMode) =>
                    prevMode === "light" ? "dark" : "light"
                  )
                }
              />
            </ListItemButton>
          </ListItem>
        </List>

        <Add/>
        {/* <Stack
          width={180}
          height={100}
          justifyContent={"center"}
          bgcolor={"white"}
        >
          <Box flex={2} bgcolor={"blue"}></Box>
          <Box flex={4} alignSelf={"center"} pb={2} pr={1}>
            <ChangeHistory
              sx={{
                position: "relative",
                width: 40,
                height: 40,
                left: 25,
                top: 5,
                color: "blue",
              }}
            />
            <ChangeHistory
              sx={{
                position: "relative",
                width: 40,
                height: 40,
                right: 14,
                top: 14,
                color: "blue",
                transform: "rotate(180deg);",
              }}
            />
          </Box>
          <Box flex={2} bgcolor={"blue"}></Box>
        </Stack> */}
      </Box>
    </Box>
  );
};

export default Sidebar;
