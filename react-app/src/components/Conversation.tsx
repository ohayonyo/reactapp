import {
  Avatar,
  Divider,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import React from "react";


type SenderType = {
  img: string;
  name: string;
};

type MessageType = {
  title: string;
  content: string;
};

type ConversationProps = {
  sender: SenderType;
  message: MessageType;
};

const Conversation = ({ sender, message }: ConversationProps) => {
  return (
    <ListItem alignItems="flex-start">
      <ListItemAvatar>
        <Avatar alt={sender.name} src={sender.img} />
      </ListItemAvatar>
      <ListItemText
        primary={message.title}
        secondary={
          <React.Fragment>
            <Typography
              sx={{ display: "inline" }}
              component="span"
              variant="body2"
              color="text.primary"
            >
              {sender.name}
            </Typography>
            {message.content}
          </React.Fragment>
        }
      />
    </ListItem>
  );
};

export default Conversation;
