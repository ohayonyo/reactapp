import {
  CheckBox,
  Favorite,
  FavoriteBorder,
  MoreVert,
  Share,
} from "@mui/icons-material";
import {
  Avatar,
  Box,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  IconButton,
  Typography,
  Checkbox,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import Post from "./Post";
import axios from "axios";

import { PostProps } from "./Post";

export type NestPostType = {
  postID:number;
  avatarDetails:{
    backgroundColor:string;
    name:string;
  };
  title:string;
  subheader:string;
  image:string;
  text:string;
}


const Feed = () => {
  const [posts, setPosts] = useState<PostProps[]>([]);

  useEffect(() => {
    const fetchPosts = async () => {
      console.log('React sends request to the nestApp to get all posts')
      axios
        .get("http://localhost:5000/posts")
        .then((response) => {
          console.log("Received response from nestApp with the following response:", response);
          if(response.status === 200){
            console.log('status 200');
            console.log('response.data=',response.data);
            const convertedData = response.data.map((nestPost:NestPostType) => {
              return {
                postID:nestPost.postID,
                avatar: (
                  <Avatar sx={{bgcolor:nestPost.avatarDetails.backgroundColor}}>
                    {nestPost.avatarDetails.name}
                  </Avatar>
                ),
                title:nestPost.title,
                subheader:nestPost.subheader,
                image:nestPost.image,
                text:nestPost.text,
              } 
            });
            console.log('converted data:',convertedData)
            setPosts(convertedData);
          }
          
        })
        .catch((error) => {
          // Handle errors
          console.error("Error fetching data:", error);
        });
    };

    fetchPosts();
  }, []);

  return (
    <Box flex={4} p={2}>
      {posts.map((post) => (
        <Post
          key={post.postID}
          postID={post.postID}
          avatar={post.avatar}
          title={post.title}
          subheader={post.subheader}
          image={post.image}
          text={post.text}
        />
      ))}
    </Box>
  );
};

export default Feed;
