import React from "react";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import SvgIcon from "@mui/material/SvgIcon";
import { SvgIconTypeMap } from "@mui/material";

type SidebarOptionPropsType = {
  name: string;
  href: string;
  icon: React.JSX.Element;
};

const SidebarOption = ({ name, href, icon }: SidebarOptionPropsType) => {
  return (
    <ListItem disablePadding>
      <ListItemButton component="a" href={href}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={name} />
      </ListItemButton>
    </ListItem>
  );
};

export default SidebarOption;
